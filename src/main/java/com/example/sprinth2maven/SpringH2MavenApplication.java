package com.example.sprinth2maven;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.sql.DataSource;
import java.sql.Connection;

@SpringBootApplication
public class SpringH2MavenApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringH2MavenApplication.class, args);
	}

	public void initDatabaseUsingSpring(@Autowired DataSource ds) {
		try (Connection conn = ds.getConnection()) {
			conn.createStatement().execute("CREATE TABLE IF NOT EXISTS countries (\n" +
					"    id BIGINT PRIMARY KEY AUTO_INCREMENT,\n" +
					"    name VARCHAR(255) NOT NULL\n" +
					");");
			conn.createStatement().execute("INSERT INTO countries (name) VALUES ('USA'), ('France'), ('Brazil'), ('Italy'), ('Canada');");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
